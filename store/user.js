import Vue from 'vue';

import {
  getUserInfo,
  saveUserInfo,
  getUserAccountInfo
} from '../service/deliroom-service';

export const state = () => ({
  user: {},
  accountInfo: []
});

export const getters = {
  balance: ({ accountInfo }) => accountInfo.length ? Math.abs(accountInfo[accountInfo.length - 1].value) : 0
};

export const actions = {
  async getUser({ commit, dispatch }) {
    const userInfo = await getUserInfo();

    if (!userInfo.data) {
      Vue.notify({
        group: 'errors',
        type: 'error',
        title: `Ошибка`,
        text: 'Произошла ошибка выполнения запроса',
      });

      return;
    }

    await dispatch('getAccountInfo');

    commit('setUser', userInfo.data);
  },

  async saveUser({ state, commit }, user) {
    const { name, surname, email } = user;

    if (state.user.name === name && state.user.surname === surname && state.user.email === email) return;

    const savedUser = await saveUserInfo({ name, surname, email });

    if (savedUser.message || savedUser.data.errors) {

      Vue.notify({
        group: 'errors',
        type: 'error',
        title: `Ошибка`,
        text: savedUser.message || 'Указанный email уже используется',
      });

      if (savedUser.data && savedUser.data.errors) {
        commit('setUser', { phone: state.user.phone, name, surname, email: '' })
      }

    } else {

      Vue.notify({
        group: 'success',
        type: 'success',
        text: 'Сохранение прошло успешно',
      });

      commit('setUser', savedUser.data);
    }
  },

  async getAccountInfo({ commit }) {
    const accountInfo = await getUserAccountInfo();

    if (!accountInfo.data) {
      Vue.notify({
        group: 'errors',
        type: 'error',
        title: `Ошибка`,
        text: 'Произошла ошибка выполнения запроса',
      });

      return;
    }

    commit('setAccountInfo', accountInfo.data)
  },
};

export const mutations = {
  setUser(state, user) {
    state.user = user;
  },

  removeUser(state) {
    state.user = {};
    state.accountInfo = [];
  },

  setAccountInfo(state, accountInfo) {
    state.accountInfo = accountInfo;
  }
};
