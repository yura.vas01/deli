import { getFilters } from '../service/deliroom-service';

export const state = () => ({
  filters: []
});

export const mutations = {
  setFilters(state, payload) {
    state.filters = payload.filters;
  }
};

export const actions = {
  async getFilters({ commit }) {
    commit('setFilters', { filters: await getFilters() })
  }
};
