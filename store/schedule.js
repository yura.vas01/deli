import Vue from 'vue';

import { createBooking, deleteBooking, updateBooking } from '../service/deliroom-service';
import bookings from "@/pages/personal/bookings";
import {getFloatTime} from "assets/js/helpers/formatTime";

export const state = () => ({
  bookings: [],
});

export const actions = {
  async addBooking({ commit }, payload) {

    let startDate = new Date(payload.booking.date);
    let endDate = new Date(payload.booking.date);

    endDate.setHours(payload.booking.timeTo.label.match(/\d+/g)[0], payload.booking.timeTo.label.match(/\d+/g)[1]);
    startDate.setHours(payload.booking.timeFrom.label.match(/\d+/g)[0], payload.booking.timeFrom.label.match(/\d+/g)[1]);

    if (payload.booking.timeTo.value <= payload.booking.timeFrom.value) {
      endDate = new Date(new Date(endDate).getTime() + 86400000)
      endDate.setHours(payload.booking.timeTo.label.match(/\d+/g)[0], payload.booking.timeTo.label.match(/\d+/g)[1]);
    }

    const currentDate = new Date();

    if (startDate < currentDate) {
      Vue.notify({
        group: 'errors',
        type: 'error',
        title: `Ошибка`,
        text: 'Нельзя бронировать прошедшее время',
      });

      return
    }

    const bookingsParams = payload.booking.visitors ? {
      place_id: payload.roomId,
      start: startDate.toISOString(),
      end: endDate.toISOString(),
      visitors: payload.booking.visitors
    } : {
      place_id: payload.roomId,
      start: startDate.toISOString(),
      end: endDate.toISOString(),
    }
    const createdBooking = await createBooking(bookingsParams);

    if (createdBooking.message || createdBooking.data.errors) {
      Vue.notify({
        group: 'errors',
        type: 'error',
        title: `Ошибка`,
        text: createdBooking.message || createdBooking.data.errors[0],
      });

      return;
    }

    if (!createdBooking.data.price || !createdBooking.data.id) {
      Vue.notify({
        group: 'errors',
        type: 'error',
        title: `Ошибка`,
        text: 'Данное время уже занято',
      });

      return;
    }

    commit('addBooking', {
      roomId: payload.roomId,
      booking: {
        ...payload.booking,
        timeTo: {
          label: (payload.booking.timeTo.value === 0) || (payload.booking.timeTo.value === 24) ? payload.booking.timeTo.label = '24:00' : payload.booking.timeTo.label,
          value: payload.booking.timeTo.value
        },
        price: createdBooking.data.price,
        price_old: createdBooking.data.price_old,
        bookingId: createdBooking.data.id,
        visitors: createdBooking.data.visitors
      },
    })
  },

  async removeBooking({ commit }, payload) {
    const deletedBooking = await deleteBooking(payload.bookingId);

    if (deletedBooking.message) {
      Vue.notify({
        group: 'errors',
        type: 'error',
        title: `Ошибка`,
        text: deletedBooking.message,
      });

      return;
    }

    commit('removeBooking', payload);
  },

  async editBooking({ commit }, payload) {
    let startDate = new Date(payload.bookingInfo.date);
    let endDate = new Date(payload.bookingInfo.date);


    endDate.setHours(payload.bookingInfo.timeTo.label.match(/\d+/g)[0], payload.bookingInfo.timeTo.label.match(/\d+/g)[1]);
    startDate.setHours(payload.bookingInfo.timeFrom.label.match(/\d+/g)[0], payload.bookingInfo.timeFrom.label.match(/\d+/g)[1]);

    if (payload.bookingInfo.timeTo.value <= payload.bookingInfo.timeFrom.value) {
      endDate = new Date(new Date(endDate).getTime() + 86400000)
      endDate.setHours(payload.bookingInfo.timeTo.label.match(/\d+/g)[0], payload.bookingInfo.timeTo.label.match(/\d+/g)[1]);
    }

    const bookingsParams = payload.bookingInfo.visitors ? {
      start: startDate.toISOString(),
      end: endDate.toISOString(),
      visitors: payload.bookingInfo.visitors
    } : {
      start: startDate.toISOString(),
      end: endDate.toISOString(),
    }
    const updatedBooking = await updateBooking(payload.bookingId, bookingsParams)

    if (updatedBooking.message || updatedBooking.data.errors) {
      Vue.notify({
        group: 'errors',
        type: 'error',
        title: `Ошибка`,
        text: updatedBooking.message || updatedBooking.data.errors[0],
      });

      return;
    }

    commit('editBooking', {
      ...payload,
      bookingInfo: {
        ...payload.bookingInfo,
        timeTo: {
          label: (payload.bookingInfo.timeTo.value === 0) || (payload.bookingInfo.timeTo.value === 24) ? '24:00' : payload.bookingInfo.timeTo.label,
          value: (payload.bookingInfo.timeTo.value === 0) ? 24 : payload.bookingInfo.timeTo.value,
        },
        price: updatedBooking.data.price,
        price_old: updatedBooking.data.price_old,
        visitors: updatedBooking.data.visitors
      }
    })
  }
};

export const mutations = {
  addBooking(state, { roomId, booking }) {
    const currentRoomBookings = state.bookings.filter((booking) => Number(booking.roomId) === Number(roomId))[0];

    if (currentRoomBookings) {
      currentRoomBookings.bookingItems = [ ...currentRoomBookings.bookingItems, booking];
    } else {
      state.bookings = [ ...state.bookings, {
        roomId,
        bookingItems: [ booking ]
      }]
    }
  },

  removeBooking(state, { roomId, bookingId }) {
    const currentRoomBookings = state.bookings.filter((booking) => Number(booking.roomId) === Number(roomId))[0];

    if (currentRoomBookings) {
      currentRoomBookings.bookingItems = currentRoomBookings.bookingItems.filter((item) => Number(item.bookingId) !== Number(bookingId));
    }
  },

  editBooking(state, { roomId, bookingId, bookingInfo }) {
    const currentRoomBookings = state.bookings.filter((booking) => Number(booking.roomId) === Number(roomId))[0];

    currentRoomBookings.bookingItems = currentRoomBookings.bookingItems
      .map((booking) => {
        if (Number(booking.bookingId) === Number(bookingId)) {
          return {
            ...booking,
            ...bookingInfo
          };
        }

        return booking;
      })
  },

  fillStoreByOwnBookings(state, payload) {
    const currentRoomBookings = state.bookings
      .filter((booking) => Number(booking.roomId) === Number(payload.roomId))[0];

    if (currentRoomBookings) {
      currentRoomBookings.bookingItems = [ ...payload.bookings ]
    } else {
      state.bookings = [ ...state.bookings, {
        roomId: payload.roomId,
        bookingItems: [
          ...payload.bookings
        ]
      }]
    }
  }
};


