import Vue from 'vue';

export const state = () => ({
  isMobile: false
});

export const mutations = {
  changeScreenType(state, payload) {
    state.isMobile = payload.screenWidth < 501;
  }
};

export const actions = {
  async nuxtServerInit({ commit, dispatch }, { app, req }) {
    Vue.$cookiz = app.$cookiz;

    Vue.host = req.headers.host;

    if (app.$cookiz.get('user-token')) {
      commit('auth/setToken', app.$cookiz.get('user-token'));

      await dispatch('user/getUser');
    }
  }
};

