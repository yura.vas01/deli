import Cookies from 'js-cookie';

import { login, logout, refreshToken } from '../service/deliroom-service';

export const state = () => ({
  token: '',
  refreshTimer: null,
  tokenRefreshing: false,
});

export const getters = {
  isAuthenticated: (state) => !!state.token,
};

export const actions = {
  async authRequest({ commit, dispatch }, authData) {
    const res = await login(authData);

    if (res.error) {
      return res;
    }

    if (res.token) {
      Cookies.set('user-token', res.token);
      commit('setToken', res.token);

      await dispatch('user/getUser', null, { root: true });

      dispatch('startRefreshing');
    }

    return res;
  },

  async authLogout({ commit }) {
    await logout();

    Cookies.remove('user-token');
    commit('removeToken');
    commit('user/removeUser', null, { root: true });

    commit('stopRefreshingToken')
  },

  async refreshToken({ commit, dispatch }) {
    commit('beginRefreshingToken');

    const res = await refreshToken();

    if (res.token) {
      Cookies.set('user-token', res.token);
      commit('setToken', res.token);
    } else {
      dispatch('authLogout');
    }

    commit('endRefreshingToken');
  },

  startRefreshing({ commit, dispatch }) {
    const timer = setInterval(() => dispatch('refreshToken'), 900000);

    commit('refreshToken', timer);
  }
};

export const mutations = {
  setToken(state, token) {
    state.token = token;
  },

  removeToken(state) {
    state.token = '';
  },

  refreshToken(state, timer) {
    state.refreshTimer = timer
  },

  stopRefreshingToken(state) {
    clearInterval(state.refreshTimer);
  },

  beginRefreshingToken(state) {
    state.tokenRefreshing = true;
  },

  endRefreshingToken(state) {
    state.tokenRefreshing = false;
  }
};
