export default function ({ store, redirect, route }) {
  if (!store.getters['auth/isAuthenticated']) {
    return redirect('/login')
  } else {

    if (route.path === '/personal' || route.path === '/personal/')
      return redirect('/personal/profile')

  }
}
