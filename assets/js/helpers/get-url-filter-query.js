export default (filters, sorting = null, page = 1) => {
  const query = {};

  filters.forEach((filter) => {
    switch (filter.dropdownType) {
      case 'date':
        if (filter.selectedDate === null) return;

        query[filter.type] = `${Number(filter.selectedDate) / (1000 * 3600)}`;
        break;

      case 'check':
        if (!filter.activeItems.length) return;

        query[filter.type] = `${filter.activeItems.map((activeItem) => activeItem.id)}`;
        break;

      case 'range':
        if (!filter.value.length) return;

        query[filter.type] = JSON.stringify(`${filter.value}`);
        break;
    }
  });

  if (sorting) {
    query.sort = sorting;
  }

  if (page > 1) {
    query.page = page;
  }

  return query;
}
