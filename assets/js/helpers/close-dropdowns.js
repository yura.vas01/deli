export default function closeDropdowns() {
  const clickEvent = new CustomEvent('click', { detail: { needToClose: true } });
  document.dispatchEvent(clickEvent);
}
