export const getFloatTime = (time) =>
  time.match(/\d+:00/) && time.length < 5
    ? parseInt(time)
    : parseFloat(`${parseInt(time)}.${time.match(/:\d+/g)[0].substr(-2) / 60 * 100 < 10 ? '0' : ''}${time.match(/:\d+/g)[0].substr(-2) / 60 * 100}`);

export const getStringTime = (time) =>
  time === parseInt(time)
    ? `${parseInt(time) < 10 ? `0${parseInt(time)}` : parseInt(time)}:00`
    : `${parseInt(time) < 10 ? `0${parseInt(time)}` : parseInt(time)}:30`;
