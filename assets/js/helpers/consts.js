 const weekDays = [
  { short: 'Вс', long: 'Воскресенье' },
  { short: 'Пн', long: 'Понедельник' },
  { short: 'Вт', long: 'Вторник' },
  { short: 'Ср', long: 'Среда' },
  { short: 'Чт', long: 'Четверг' },
  { short: 'Пт', long: 'Пятница' },
  { short: 'Сб', long: 'Суббота' }
];

const months = ['января', 'февраля', 'марта', 'апреля', 'мая',
  'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];

const timeZoneOffsets = {
 'Europe/Moscow': {
   forCalendar: '-03:00',
   normal: '+03:00',
   float: 3
 }
};

export {
  weekDays,
  months,
  timeZoneOffsets
}
