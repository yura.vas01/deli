import { getFloatTime, getStringTime } from './formatTime';

export default (room) =>  {
  let start = getFloatTime(room.schedule[0].rules.from),
      end   = getFloatTime(room.schedule[0].rules.to) || 24;

  room.schedule.forEach((scheduleItem) => {
    const itemStart = getFloatTime(scheduleItem.rules.from);
    const itemEnd = getFloatTime(scheduleItem.rules.to) || 24;

    if (itemStart < start) start = itemStart;
    if (itemEnd > end) end = itemEnd
  });

  return {
    startTime: getStringTime(start),
    endTime: getStringTime(end)
  }
};
