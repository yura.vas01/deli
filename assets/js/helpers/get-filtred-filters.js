export default (filters, template) => {
  let filterModified = filters.map((filter) => {
    const type = filter.type;
    const neededTemplate = template.filter((template) => template.type === type)[0];

    if (!neededTemplate) return;

    const result = { ...neededTemplate, ...filter  };

    if (!result.values.length && neededTemplate.dropdownType === 'range')
      result.values = [ ...neededTemplate.values ];

    return result;
  });

  filterModified = filterModified.filter((filter) => !!filter);

  return filterModified;
}
