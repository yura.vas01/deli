module.exports = {
  server: {
    port: 8088, // default: 3000
  },
  /*
  ** Headers of the page
  */
  head: {
    title: 'deliroom',
    htmlAttrs: {
      lang: 'ru'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' },
      { hid: 'description', name: 'description', content: 'Сервис почасовой аренды' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.png' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    },
    transpile: ['vee-validate/dist/rules']
  },
  modules: [
    ['cookie-universal-nuxt', { alias: 'cookiz' }],
    '@nuxtjs/recaptcha',
    '@nuxtjs/gtm',
  ],
  gtm: {
    id: 'GTM-T668DM6',
  },
  recaptcha: {
    hideBadge: false, // Hide badge element (v3 & v2 via size=invisible)
    siteKey: '6LeOJbkZAAAAAL5tU2cwFLkxYgaHkEcGElFyjmEc', // Site key for requests
    version: 2,
    size: 'invisible', // Size: 'compact', 'normal', 'invisible' (v2)
    language: 'ru'
  },
  plugins: [
    { src: '~plugins/v-calendar.js', ssr: false },
    { src: '~plugins/swiper.js' },
    { src: '~plugins/full-calendar.js', ssr: false },
    { src: '~plugins/ymap-plugin.js', ssr: false },
    { src: '~plugins/filters.js' },
    { src: '~plugins/vue-select.js' },
    { src: '~plugins/vee-validate.js' },
    { src: '~/plugins/notifications-ssr', mode: 'server' },
    { src: '~/plugins/notifications-client', mode: 'client' }
  ],
  css: [
    { src: '~assets/scss/reset.scss', lang: 'sass' },
    { src: '~assets/scss/typography.scss', lang: 'sass' },
    { src: '~assets/scss/global.scss', lang: 'sass' },
  ],
  router: {
    extendRoutes(routes, resolve) {
      routes.unshift({
        path: '/places/:id(\\D+)',
        name: 'places-rtype',
        components: {
          default: resolve(__dirname, 'pages/places/index'),
        },
      })
    },
  }
};

