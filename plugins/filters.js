import Vue from 'vue'

Vue.filter('rubCurrency', (val) => {
  if (!val) return;

  return `${parseInt(val.toString()).toLocaleString('ru-RU')} ₽`
})
;
