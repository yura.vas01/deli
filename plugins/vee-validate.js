import { extend } from 'vee-validate';
import { required, email, regex, length } from 'vee-validate/dist/rules';
import { setInteractionMode } from 'vee-validate';

setInteractionMode('eager');

extend('required', {
  ...required,
  message: 'Это обязательное поле'
  });

extend('email', {
  ...email,
  message: 'Email введен неверно'
});

extend('regex', regex);
extend('length', length);
