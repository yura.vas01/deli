import Vue from 'vue'
import YmapPlugin from 'vue-yandex-maps'

const settings = {
  apiKey: '8c4d2163-6e60-4aa9-9791-c64031c228bc',
  lang: 'ru_RU',
  coordorder: 'latlong',
  version: '2.1'
}; // настройки плагина

Vue.use(YmapPlugin, settings);
