import Vue from 'vue';
import Cookies from 'js-cookie';

const apiBase = '/api/common/v1';

let headers = {
  'Accept': 'application/json',
};

const getResource = async (method, url, params = {}, options = {}) => {
  const host = Vue.host
    ? Vue.host.includes('deliroom.ru') && !Vue.host.includes('dev.')
      ? 'deliroom.ru'
      : 'dev.deliroom.ru'
    : location.host.includes('deliroom.ru') && !location.host.includes('dev.')
      ? 'deliroom.ru'
      : 'dev.deliroom.ru';

  const requestUrl = new URL(`https://${host}${apiBase}${url}`);
  let res = null;
  let fd = null;

  const token = Vue.$cookiz ? Vue.$cookiz.get('user-token') : Cookies.get('user-token');

  if (typeof FormData !== 'undefined' && options.useFormData) {
    fd = Object.entries(params).reduce((fd, [ k, v ]) => (fd.append(k, v), fd), new FormData);
  }

  if (!options.useFormData) {
    headers['Content-Type'] = 'application/json';
  } else {
    delete headers['Content-Type'];
  }

  if (token) {
    headers = {
      ...headers,
      'authorization': `Bearer ${token}`
    }
  }

  if (method === 'GET' || method === 'DELETE') {
    Object.entries(params)
      .forEach(([key, param]) => requestUrl.searchParams.append(key, param));

    res = await fetch(requestUrl, { method, headers, mode: 'cors' })
  } else {
    res = await fetch(requestUrl, { method, headers, body: options.useFormData ? fd : JSON.stringify(params) })
  }

  if (res.status === 204) {
    return '';
  }

  if (res.status === 404) {
    return { error: 'no-found' }
  }

  return res.headers.get('authorization')
    ? { body: await res.json(), token: res.headers.get('authorization') }
    : await res.json();
};

const getFilters = async () => {
  const res = await getResource(`GET`, `/filters`);
  return res.data
};

const getRooms = async (params) => {
  const res = await getResource(`GET`, `/places`, params);
  return res;
};

const getRoom = async (id) => {
  const res = await getResource(`GET`, `/places/${id}`);
  return res.data;
};

const getBookings = async (id) => {
  const res = await getResource(`GET`, `/places/${id}/bookings`);
  return res.data;
};

const getSeoBySlug = async (slug) => {
  const res = await getResource(`GET`, `/pages/content`, { slug });
  return res.data;
};

const login = async (loginParams) =>
  await getResource(`POST`, `/auth/sms/login`, loginParams);

const logout = async () =>
  await getResource(`POST`, `/auth/sms/logout`);

const refreshToken = async () =>
  await getResource(`GET`, `/auth/sms/refresh`);

const getUserInfo = async () => await getResource(`GET`, `/user`);

const saveUserInfo = async (userInfo) => await getResource(`PATCH`, `/user`, userInfo);

const getUserBookings = async () => await getResource(`GET`, `/bookings`);

const getUserAccountInfo = async () => await getResource(`GET`, `/account`);

const sendFeedback = async (feedbackParams) => await getResource(`POST`, `/feedback`, feedbackParams);

const sendPartner = async (partnerParams) => await getResource(`POST`, `/affiliate`, partnerParams, { useFormData: true });

const createBooking = async (bookingParams) => await getResource(`POST`, `/bookings`, bookingParams);

const updateBooking = async (id, bookingParams) => await getResource(`PATCH`, `/bookings/${id}`, bookingParams);

const deleteBooking = async (id) => await getResource(`DELETE`, `/bookings/${id}`);

const createOrder = async (orderParams) => await getResource(`POST`, `/orders`, orderParams);

const getOrders = async () => await getResource(`GET`, `/orders`);

const getPaymentLink = async (id, params) => await getResource(`POST`, `/orders/${id}/pay`, params);

const updateOrderPayment = async (paymentId) => await getResource(`POST`, `/orders/check/${paymentId}`);

const cancelOrder = async (id) => await getResource(`POST`, `/orders/${id}/cancel`);

export {
  getFilters,
  getRooms,
  getRoom,
  getBookings,
  getSeoBySlug,
  login,
  logout,
  refreshToken,
  getUserInfo,
  saveUserInfo,
  getUserBookings,
  getUserAccountInfo,
  sendFeedback,
  sendPartner,
  createBooking,
  updateBooking,
  deleteBooking,
  createOrder,
  getPaymentLink,
  getOrders,
  updateOrderPayment,
  cancelOrder
}

